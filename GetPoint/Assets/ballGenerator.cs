using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballGenerator : MonoBehaviour
{
    public GameObject ballPrefab;
    float span = 1.0f;
    float delta = 0;



    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if(this.delta>this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(ballPrefab) as GameObject;
            int py = Random.Range(1, 2);
            go.transform.position = new Vector3(30, py,2.4f);
        }
    }
}
