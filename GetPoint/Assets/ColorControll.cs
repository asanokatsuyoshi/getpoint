using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorControll : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Color color = gameObject.GetComponent<Renderer>().material.color;
        color.a = 0f;

        gameObject.GetComponent<Renderer>().material.color = color;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Color color = gameObject.GetComponent<Renderer>().material.color;
            color.a = 0f;

            gameObject.GetComponent<Renderer>().material.color = color;
        }

        if (Input.GetMouseButtonDown(1))
        {
            Color color = gameObject.GetComponent<Renderer>().material.color;
            color.a = 1.0f;

            gameObject.GetComponent<Renderer>().material.color = color;
        }
    }
}
