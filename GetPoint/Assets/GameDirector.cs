using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    Image hpGauge;
    Image WallGauge;
    // Start is called before the first frame update
    void Start()
    {
        this.hpGauge = GameObject.Find("hpGauge").GetComponent<Image>();
        this.WallGauge = GameObject.Find("WallGauge").GetComponent<Image>();
    }

    public void DecreaseHP()
    {
        this.hpGauge.fillAmount -= 0.1f;
    }

    public void DecreaseWall()
    {
        this.WallGauge.fillAmount -= 0.1f;
    }

    void Update()
    {
        if(hpGauge.fillAmount<=0)
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }
}
