using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour
{
    public float speed;
    void Start()
    {
        DontDestroyOnLoad(gameObject); //シーンを切り替えても削除しない
        speed = 5;
    }

    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Goal")
        {
            SceneManager.LoadScene("ClearScene");
        }
    }
}
