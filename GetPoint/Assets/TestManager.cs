using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestManager : MonoBehaviour
{
    public GameObject ClickWallPrefab;
    public GameObject target;
    private Vector3 distance;

    void Start()
    {
        distance = transform.position - target.transform.position;
    }

    void Update()
    {

    }

    void LateUpdate()
    {
        transform.position = target.transform.position + distance;
    }
}
